package webx;

import stdx.Utils;

public class LogFile{
	public static final int DBG = 0;
	public static final int TIP = 1;
	public static final int INF = 2;
	public static final int IMP = 3;
	public static final int ERR = 4;

    public static String GetLogHead(int level){
        long id = Thread.currentThread().getId() % 1000000;
        String time = Utils.GetDateTimeString().replaceAll("-", "");

        switch(level){
            case DBG: return String.format("[%s|DBG|%06d] ", time, id);
            case TIP: return String.format("[%s|TIP|%06d] ", time, id);
            case IMP: return String.format("[%s|IMP|%06d] ", time, id);
            case ERR: return String.format("[%s|ERR|%06d] ", time, id);
            default: return String.format("[%s|INF|%06d] ", time, id);
        }
    }
    public static void Error(Exception error){
        Trace(ERR, Utils.GetStackString(error));
    }
    public static void Trace(Exception error){
        Trace(INF, Utils.GetStackString(error));
    }
    public static void Debug(Exception error){
        Trace(DBG, Utils.GetStackString(error));
    }
	public static void Error(String fmt, Object...args){
		Trace(ERR, fmt, args);
	}
	public static void Trace(String fmt, Object...args){
		Trace(INF, fmt, args);
	}
	public static void Debug(String fmt, Object...args){
		Trace(DBG, fmt, args);
	}
	public static void Trace(int level, String fmt, Object...args){
	    String msg = (args == null || args.length == 0) ? fmt : String.format(fmt, args);

		try{
		    WebApp.Trace(level, msg);
		}
		catch (UnsatisfiedLinkError e){
		    synchronized(LogFile.class){
                System.out.println(GetLogHead(level) + msg);
            }
        }
		catch(Exception e){
			e.printStackTrace();
		}
	}
}