#include <webx/menu.h>
#include <dbentity/T_XG_CGI.h>

class EditCgi : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(EditCgi)

int EditCgi::process()
{
	param_int(maxsz);
	param_int(maxcnt);
	param_int(enabled);
	param_string(path);
	param_string(flag);
	param_string(remark);
	param_int(hostmaxcnt);

	path = CgiMapData::GetKey(path);

	if (path.empty() || enabled < CGI_DISABLE || enabled > CGI_PUBLIC) return simpleResponse(XG_PARAMERR);

	checkLogin();
	checkSystemRight();

	int res = 0;
	CT_XG_CGI tab;
	sp<DBConnect> dbconn = webx::GetDBConnect();

	tab.init(dbconn);

	tab.path = path;
	tab.maxsz = maxsz;
	tab.maxcnt = maxcnt;
	tab.remark = remark;
	tab.enabled = enabled;
	tab.statetime.update();
	tab.hostmaxcnt = hostmaxcnt;

	if (flag == "D")
	{
		res = tab.remove();

		if (res >= 0) app->removeCgiAccess(path);
	}
	else if (flag == "U")
	{
		res = tab.insert();

		if (res < 0) res = tab.update();
	}
	else if (flag == "A")
	{
		if (tab.find() && tab.next())
		{
			res = XG_DUPLICATE;
		}
		else
		{
			res = tab.insert();
		}
	}
	else
	{
		res = XG_PARAMERR;
	}

	if (res >= 0) app->updateCgiData(path);

	json["code"] = res;
	out << json;
	
	return XG_OK;
}