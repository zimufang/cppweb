#include <webx/menu.h>
#include <dbentity/T_XG_NOTE.h>

class GetNoteList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetNoteList)

int GetNoteList::process()
{
	param_string(id);
	param_string(user);
	param_string(level);
	param_string(title);
	param_string(folder);

	checkLogin();

	string dbid;
	string sqlcmd;

	user = this->user;
	
	if (webx::IsAlnumString(id))
	{
		dbid = this->dbid;
		stdx::format(sqlcmd, "SELECT ID,TYPE,ICON,LEVEL,TITLE,FOLDER,STATETIME FROM T_XG_NOTE WHERE USER='%s' AND ID='%s' AND LENGTH(TITLE)>0", user.c_str(), id.c_str());
	}
	else if (folder.empty())
	{
		dbid = this->dbid;
		stdx::format(sqlcmd, "SELECT ID,TYPE,ICON,LEVEL,TITLE,FOLDER,STATETIME FROM T_XG_NOTE WHERE USER='%s' AND LENGTH(TITLE)>0 ORDER BY FOLDER ASC,POSITION ASC", user.c_str());
	}
	else if (webx::IsFileName(title))
	{
		stdx::format(sqlcmd, "SELECT ID,TYPE,ICON,LEVEL,TITLE,FOLDER,STATETIME FROM T_XG_NOTE WHERE (USER='%s' OR LEVEL>2) AND FOLDER='WEBPAGE' AND TITLE='%s' AND LENGTH(TITLE)>0", user.c_str(), title.c_str());
	}
	else
	{
		webx::CheckFileName(folder);

		if (folder == "WEBPAGE" && user == "system")
		{
			stdx::format(sqlcmd, "SELECT ID,TYPE,ICON,LEVEL,TITLE,FOLDER,STATETIME FROM T_XG_NOTE WHERE FOLDER='WEBPAGE' AND LENGTH(TITLE)>0 ORDER BY POSITION ASC");
		}
		else
		{
			dbid = this->dbid;
			
			if (IsNumberString(level.c_str()))
			{
				stdx::format(sqlcmd, "SELECT ID,TYPE,ICON,LEVEL,TITLE,FOLDER,STATETIME FROM T_XG_NOTE WHERE USER='%s' AND FOLDER='%s' AND LENGTH(TITLE)>0 AND LEVEL=%s ORDER BY POSITION ASC", user.c_str(), folder.c_str(), level.c_str());
			}
			else
			{
				stdx::format(sqlcmd, "SELECT ID,TYPE,ICON,LEVEL,TITLE,FOLDER,STATETIME FROM T_XG_NOTE WHERE USER='%s' AND FOLDER='%s' AND LENGTH(TITLE)>0 ORDER BY POSITION ASC", user.c_str(), folder.c_str());
			}
		}
	}
	
	sp<DBConnect> dbconn = webx::GetDBConnect(dbid);
	
	if (dbid.length() > 0) json["dbid"] = dbid;

	json["code"] = webx::PackJson(dbconn->query(sqlcmd), "list", json);
	out << json;
	
	return XG_OK;
}