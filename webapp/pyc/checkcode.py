import stdx;
import imgx;


def main(app):
	type = app.getParameter('type');
	if type == 'L' or type == 'R' or type == 'P' or type == 'DOC':
		img = imgx.CheckCode();
		code = img.getText().lower();
		if app.setSession(type + '_CHECKCODE', code) >= 0:
			app.setContentType(app.getMimeType(img.getFormat()));
			return img.draw();
	return stdx.XG_ERROR;
