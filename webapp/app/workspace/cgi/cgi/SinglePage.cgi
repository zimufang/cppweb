<%@ path=${filename}%>
<%
	param_string(path);
	param_string(icon);
	param_string(title);
	param_string(align);
	param_string(footer);
	param_string(padding);
	param_string(background);

	stdx::tolower(align);

	if (path.empty()) return XG_FAIL;

	if (icon.empty()) icon = "/favicon.ico";

	string client = stdx::tolower(request->getHeadValue("User-Agent"));

	if (client.find("android") == string::npos && client.find("iphone") == string::npos && client.find("ipad") == string::npos)
	{
		if (padding.empty()) padding = "1vh 6vw";

		if (footer.empty()) footer = "/ShareNote?flag=S&title=FOOTER";
	}
	else
	{
		if (padding.empty()) padding = "1vh 3vw";
	}

	if (footer == "none") footer.clear();

	if (background.empty()) background = "none";
%>
<!DOCTYPE HTML>
<html manifest='/res/etc/manifest.cache'>
<head>
<title><%=title%></title>
<meta name='referrer' content='always'/>
<meta name='keywords' content='<%=title%>'/>
<meta name='description' content='<%=title%>'/>
<link id='IconLink' rel='shortcut icon' href='<%=icon%>'/>
<meta http-equiv='x-ua-compatible' content='ie=edge,chrome=1'/>
<meta http-equiv='content-type' content='text/html; charset=utf-8'/>
<meta name='viewport' content='width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no'/>

<script>
if (typeof(require) == 'function') delete window.module;
</script>

<script src='/res/lib/utils.js.gzip'></script>
<script src='/res/lib/laydate/laydate.js.gzip'></script>
<script src='/res/lib/highlight/highlight.js.gzip'></script>
<script src='/res/lib/kindeditor/kindeditor.js.gzip'></script>
<script src='/res/lib/kindeditor/lang/zh-cn.js.gzip'></script>

<link rel='stylesheet' type='text/css' href='/app/workspace/css/base.css'/>
<link rel='stylesheet' type='text/css' href='/app/workspace/css/menupad.css'/>
<link rel='stylesheet' type='text/css' href='/app/workspace/css/notepad.css'/>
<link rel='stylesheet' type='text/css' href='/res/lib/highlight/css/tomorrow.css'/>

<style>
body{
	background: <%=background%>;
	background-attachment: fixed;
}
#SinglePageDiv{
<%if (align.length() > 0){%>
	text-align: <%=align%>;
<%}%>
<%if (padding.length() > 0){%>
	padding: <%=padding%>;
<%}%>
}
#SinglePageSpan{
	text-align: left;
}
</style>

<script>
var saveneeded = null;
var automodifymap = {};
var modifyNotepad = null;

function getMainView(){
	return $('#SinglePageDiv');
}
function getFloatView(){
	return $('#FloatViewDiv');
}
function restoreContentSize(){
	$('#SinglePageDiv').width('100%');
}
function setSaveNeeded(needed){
	saveneeded = needed;
}
function autoModifyContentSize(id){
	automodifymap[id] = id;
}
function showFloatQRCodeView(text, title, size){
	if (size == null) size = 120;

	var cx = size + 13;
	var cy = size + 30;
	var msg = "<div id='XG_FLOAT_CLOSE_BUTTON'></div><div id='XG_FLOAT_QRCODE_DIV'><table><tr><td id='XG_FLOAT_QRCODE_TD'><v-qrcode id='XG_FLOAT_QRCODE_IMAGE'></v-qrcode></td></tr>";

	if (text == null) text = window.location.href;

	if (title == null) title = '手机用户扫码访问';

	if (title.length > 0){
		msg += "<tr><td style='color:#678;text-align:center;padding-top:2px'>" + title + "</td></tr>";
		cy += 16;
	}

	msg += "</table></div>";

	getFloatView().css('background', '#FFF').css('border', '1px solid #CDE').css('box-shadow', '2px 4px 6px #554433').width(cx).height(cy).html(msg).show();

	getVue('XG_FLOAT_QRCODE_DIV');

	$('#XG_FLOAT_CLOSE_BUTTON').click(function(){
		getFloatView().hide();
	});
	
	$('#XG_FLOAT_QRCODE_TD').width(size);

	$('#XG_FLOAT_QRCODE_IMAGE').val(text).attr('background', '#FFF').attr('foreground', '#000').attr('size', size).click();
	
	return getFloatView().show();
}

window.onload = function(){
	$('#GotoTopDiv').hide().click(function(){
		getCtrl('SinglePageDiv').scrollTop = 0;
		$('#GotoTopDiv').fadeOut();
	});

	setCurrentUser('<%=user%>');

	$(window).resize(function(){
		if (modifyNotepad) modifyNotepad();
	});

	var path = '<%=path%>';
	var icon = '<%=icon%>';
	var title = '<%=title%>';

	var msg = getHttpResult(path);

	if (getFileExtname(path).indexOf('md') >= 0){
		marked.setOptions({
			highlight: function(code){
				return hljs.highlightAuto(code).value;
			}
		});

		msg = marked(msg);
	}
	$('#SinglePageSpan').html(msg);
<%if (footer.length() > 0){%>
	$('#SinglePageFooterDiv').load('<%=footer%>');
<%}%>
}
</script>
</head>
<body>
	<div id='GotoTopDiv'></div>
	<div id='FloatViewDiv'></div>
	<div id='SinglePageDiv'>
		<span id='SinglePageSpan'></span>
	</div>
	<div id='SinglePageFooterDiv'></div>
</body>
</html>