#include "T_XG_PARAM.h"


void CT_XG_PARAM::clear()
{
	this->id.clear();
	this->name.clear();
	this->param.clear();
	this->filter.clear();
	this->remark.clear();
	this->statetime.clear();
}
int CT_XG_PARAM::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_PARAM(" + string(GetColumnString()) + ") VALUES(";
	sql += this->id.toValueString(conn->getSystemName());
	vec.push_back(&this->id);
	sql += ",";
	sql += this->name.toValueString(conn->getSystemName());
	vec.push_back(&this->name);
	sql += ",";
	sql += this->param.toValueString(conn->getSystemName());
	vec.push_back(&this->param);
	sql += ",";
	sql += this->filter.toValueString(conn->getSystemName());
	vec.push_back(&this->filter);
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_PARAM::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->id = row->getString(0);
	this->id.setNullFlag(row->isNull());
	this->name = row->getString(1);
	this->name.setNullFlag(row->isNull());
	this->param = row->getString(2);
	this->param.setNullFlag(row->isNull());
	this->filter = row->getString(3);
	this->filter.setNullFlag(row->isNull());
	this->remark = row->getBinary(4);
	this->remark.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(5);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_PARAM::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_PARAM";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_PARAM::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return find(getPKCondition(), vec);
}
string CT_XG_PARAM::getPKCondition()
{
	string condition;
	condition = "ID=";
	condition += this->id.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_PARAM::update(bool nullable)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_PARAM SET ";
	if (nullable || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (nullable || !this->param.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PARAM=";
		sql += this->param.toValueString(conn->getSystemName());
		v.push_back(&this->param);
	}
	if (nullable || !this->filter.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "FILTER=";
		sql += this->filter.toValueString(conn->getSystemName());
		v.push_back(&this->filter);
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->id);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_PARAM::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_PARAM";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_PARAM::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return remove(getPKCondition(), vec);
}
string CT_XG_PARAM::getValue(const string& key)
{
	if (key == "ID") return this->id.toString();
	if (key == "NAME") return this->name.toString();
	if (key == "PARAM") return this->param.toString();
	if (key == "FILTER") return this->filter.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_PARAM::setValue(const string& key, const string& val)
{
	if (key == "ID")
	{
		this->id = val;
		return true;
	}
	if (key == "NAME")
	{
		this->name = val;
		return true;
	}
	if (key == "PARAM")
	{
		this->param = val;
		return true;
	}
	if (key == "FILTER")
	{
		this->filter = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_PARAM::update(const string& condition, bool nullable, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_PARAM SET ";
	if (nullable || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (nullable || !this->param.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PARAM=";
		sql += this->param.toValueString(conn->getSystemName());
		v.push_back(&this->param);
	}
	if (nullable || !this->filter.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "FILTER=";
		sql += this->filter.toValueString(conn->getSystemName());
		v.push_back(&this->filter);
	}
	if (nullable || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (nullable || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
